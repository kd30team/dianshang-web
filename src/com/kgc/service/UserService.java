package com.kgc.service;

import com.kgc.entity.User;

import java.util.List;

public interface UserService {

    /**
     * 根据用户名查询用户对象
     * @param username
     * @return
     */
    public User getUserByUsername(String username);

    /**
     * 分页展示用户列表数据
     * @return
     * @param name
     * @param userRole
     * @param currentPage
     */
    public List<User> listUsers(String name, String userRole, int currentPage);

    /**
     * 查询用户记录数
     * @param name
     * @param userRole
     * @return
     */
    public int countUsers(String name, String userRole);

    /**
     * 根据id查询用户对象
     * @param id
     * @return
     */
    public User getUserById(int id);

    /**
     * 添加用户
     * @param u
     * @return
     */
    public int insertUser(User u);

    /**
     * 根据id删除用户
     * @param id
     * @return
     */
    public int deleteUserById(int id,String rootPath);


    /**
     * 修改
     * @param u
     * @return
     */
    public int updateUser(User u);

    /**
     * 根据用户编号查询数量
     * @param usercode
     * @return
     */
    public int countUserByUsercode(String usercode);

}
