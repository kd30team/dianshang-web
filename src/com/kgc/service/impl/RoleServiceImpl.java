package com.kgc.service.impl;

import com.kgc.dao.RoleDao;
import com.kgc.entity.Role;
import com.kgc.service.RoleService;
import com.kgc.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    @Transactional(readOnly = true)
    @Override
    public List<Role> listRole() {

        return roleDao.listRole();
    }
}
