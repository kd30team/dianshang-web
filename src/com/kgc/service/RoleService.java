package com.kgc.service;

import com.kgc.entity.Role;

import java.util.List;

public interface RoleService {
    /**
     * 查询角色列表
     * @return
     */
    public List<Role> listRole();
}
