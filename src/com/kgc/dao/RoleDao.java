package com.kgc.dao;

import com.kgc.entity.Role;

import java.util.List;

public interface RoleDao {

    /**
     * 查询角色列表
     * @return
     */
    public List<Role> listRole();
}
