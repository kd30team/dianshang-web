package com.kgc.controller;

import com.alibaba.fastjson.JSONObject;
import com.kgc.entity.Role;
import com.kgc.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@Controller
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 异步获取用户角色列表
     * @throws IOException
     */
    @RequestMapping(value = "listRole",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public String listRole() throws IOException {
        //获取数据
        List<Role> list = roleService.listRole();
        //转json
        String json = JSONObject.toJSONString(list);
        return json;

    }

}
