package com.kgc.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.kgc.entity.User;
import com.kgc.service.UserService;
import com.kgc.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    /**
     * 接受日期格式字符串  进行数据转换 解决方案
     * 1.@DateTimeFormat
     * 2.@InitBinder  初始化绑定解决
     * 3.编写一个自定义转换器实现   1.编写一个转换器类DateConverter  2.注册该类
     */

//    @InitBinder   //创建该类 执行该方法
//    private void initBinder(DataBinder binder){
//        //注册自定义编辑器
//        binder.registerCustomEditor(Date.class,new DateEditor());
//    }
//
//    private class DateEditor extends PropertyEditorSupport{
//        @Override
//        public void setAsText(String text) throws IllegalArgumentException {
//            try {
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//                Date d = sdf.parse(text);
//                this.setValue(d);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        }
//    }



    @Autowired
    private UserService userService;

    /**
     * 登录
     * @param session
     * @param username
     * @param userpassword
     * @param model
     * @return
     */
    @RequestMapping("/login")
    public String login(HttpSession session,String username,String userpassword,Model model){
        //调用service获取数据
        User user = userService.getUserByUsername(username);
        //原来的跳转地址  由于视图解析器的存在   跳转时将加上前缀和后缀 造成地址不存在
//        String resultView = "index";
        //视图解析器将不起作用
        String resultView = "forward:index.jsp";

        //故意放出的一些问题
//        String str = null;
//        str.length();

        if(user != null){
            //判定密码
            String sysPassword = user.getUserpassword();
            if(sysPassword.equals(userpassword)){
                //密码正确
                resultView = "frame";
                //用户登录信息存放session
                session.setAttribute("loginUser",user);
            }else{
                //密码错误
                model.addAttribute("passwordFlag","密码输入不正确");
            }
        }else{
            //用户不存在
            model.addAttribute("usernameFlag","用户不存在");
        }
        return resultView;
    }


    /**
     * 注销
     * @return
     */
    @RequestMapping("/loginout")
    public String loginout(HttpSession session){
        //移除session中登录信息
        session.removeAttribute("loginUser");
        //回首页
        return "redirect:index.jsp";
    }


    /**
     * 跳转到用户列表控制
     * @return
     */
    @RequestMapping("/toUserlist")
    public String toUserlist(){
        return "user/userlist";
    }

    /**
     * 异步查询用户列表
     * @param response
     * @param name
     * @param userRole
     * @param currentPage
     */
    @RequestMapping("/listUser")
    @ResponseBody
    public String listUser(HttpServletResponse response,String name, String userRole, int currentPage){

        //获取列表数据
        List<User> list = userService.listUsers(name,userRole,currentPage);
        //总记录数
        int totalCount = userService.countUsers(name,userRole);
        //总页数
        int totalPage = totalCount % 4 == 0 ? totalCount / 4 : totalCount / 4 + 1;
        //封装map
        Map<String,Object> resultMap = new HashMap<String,Object>();
        resultMap.put("list",list);
        resultMap.put("totalCount",totalCount);
        resultMap.put("totalPage",totalPage);

        String json = JSONObject.toJSONString(resultMap,SerializerFeature.WriteDateUseDateFormat);
        return json;
    }

    //跳转到用户添加页面
    @RequestMapping("/toUserAdd")
    public String toUserAdd(){   //@ModelAttribute("u")  User u model.addAttribute("u",u)
        return "user/useradd";
    }


    /**
     * 添加用户
     * 可以定义一个对象 直接接受页面上所有的文本框值
     * 实体类的属性名必须和表单的name一致
     * @return
     */
    @RequestMapping("/userAdd")
    public String userAdd(HttpSession session,User u, @RequestParam("cardphotoFile") MultipartFile cardphotoFile,
                          @RequestParam("workphotoFile") MultipartFile workphotoFile,
                          Model model){
        //注意点： 实体类的属性名必须和表单的name一致
        //实体类中接受文件的属性名 不可以 和文件表单的name一致
        //接受文件 使用MultipartFile对象   都需要加上一个请求参数注解@RequestParam
        //        if(br.hasErrors()){
//            //验证出现错误  返回到页面
//            return "user/useradd";
//        }
        System.out.println(u);
        ServletContext application = session.getServletContext();
        //证件照
        String cardpath = FileUtil.uploadFile(application,cardphotoFile);
        //工作照
        String workPath = FileUtil.uploadFile(application,workphotoFile);

        u.setCardphoto(cardpath);
        u.setWorkphoto(workPath);
        //调用保存
        User loginUser = (User) session.getAttribute("loginUser");
        //创建人
        u.setCreateby(loginUser.getId());
        //创建时间
        u.setCreationdate(new Date());
        int flag = userService.insertUser(u);
        model.addAttribute("insertFlag",flag);
        return "user/useradd";
    }


    //根据id 查看单个用户

    @RequestMapping("/userview/{id}")
    public String userview(@PathVariable int id,Model model){
        //获取id
        User u = userService.getUserById(id);
        model.addAttribute("user",u);
       return "user/userview";
    }

    /**
     * 跳转到用户修改页
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/toUserModify/{id}")
    public String toUserModify(@PathVariable  int id,Model model){
        //根据id获取对象
        User u = userService.getUserById(id);
        model.addAttribute("user",u);
       return "user/usermodify";
    }

    @RequestMapping("/userModify")
    public String userModify(HttpSession session,User u,
             @RequestParam("cardphotoFile") MultipartFile cardphotoFile,
             @RequestParam("workphotoFile") MultipartFile workphotoFile,
                    Model model         ){
        //获取数据库中对应User对象
        User userdb = userService.getUserById(u.getId());

        ServletContext application = session.getServletContext();
        String rootPath  = application.getRealPath("/");
        if(!cardphotoFile.isEmpty()){
            //上传新的证件照
            String newPath = FileUtil.uploadFile(application,cardphotoFile);
            //删除老的证件照
            FileUtil.deleteFile(userdb.getCardphoto(),rootPath);
            u.setCardphoto(newPath);
        }else{
            u.setCardphoto(userdb.getCardphoto());
        }

        if(!workphotoFile.isEmpty()){
            //上传新的工作照
            String newPath = FileUtil.uploadFile(application,workphotoFile);
            //删除老的工作照
            FileUtil.deleteFile(userdb.getWorkphoto(),rootPath);
            u.setWorkphoto(newPath);
        }else{
            u.setWorkphoto(userdb.getWorkphoto());
        }

        User loginUser = (User) session.getAttribute("loginUser");
        //设置修改人
        u.setModifyby(loginUser.getId());
        //修改日期
        u.setModifydate(new Date());
        int flag = userService.updateUser(u);
        model.addAttribute("updateFlag",flag);
        return "user/userlist";
    }

    @RequestMapping("checkUsercode")
    @ResponseBody
    public String checkUsercode(String usercode){
        int count = userService.countUserByUsercode(usercode);
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("count",count);
        return JSONObject.toJSONString(resultMap);
    }


    //删除用户
    @RequestMapping("/userDelete/{id}")
    public String userDelete(HttpSession session,@PathVariable int id,Model model){
        //获取应用的绝对地址
        ServletContext application = session.getServletContext();
        //获取发布项目的跟路径
        String rootPath = application.getRealPath("/");
        int flag = userService.deleteUserById(id,rootPath);
        model.addAttribute("deleteFlag",flag);
        return "user/userlist";
    }


    /**
     * 对于一些异常的处理   是在项目处于生产阶段是才使用的
     * 开发阶段时  不需要将这些代码释放
     *
     * 异常兜底的2种解决方案
     * 1.局部异常 在controller类中编写如下方法  @ExceptionHandler(Exception.class)
     * 2.全局异常  在spring配置文件中 配置即可 全局生效
     * @return
     */
//    @ExceptionHandler(Exception.class)
//    public String resolveException(){
//        return "401";   //享受视图解析器
//    }

}
