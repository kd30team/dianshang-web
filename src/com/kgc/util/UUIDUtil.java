package com.kgc.util;

import java.util.UUID;

public class UUIDUtil {


    public static String createUUID(){
        String uid = UUID.randomUUID().toString();
        uid = uid.replaceAll("-","");
        return uid;
    }

    public static void main(String[] args) {
        String uid = UUID.randomUUID().toString();
        System.out.println(uid);
    }
}
