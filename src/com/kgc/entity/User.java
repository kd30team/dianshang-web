package com.kgc.entity;

import com.alibaba.fastjson.annotation.JSONField;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 用户实体类  对应smbms_user表
 */
public class User {

    private int id;             //id
    @Length(min = 4,max = 8)
    private String usercode;    //用户编码

    private String username;       //用户名

    private String userpassword;    //密码

    private int gender;             //性别  1：男  0：女
//    @DateTimeFormat(pattern = "yyyy-MM-dd") //接受的字符串  转换成日期
//    @JSONField(format = "yyyy-MM-dd")       //ajax异步传输数据  解决日期格式问题
    private Date birthday;          //生日

    private String phone;           //手机

    private String  address;        //地址

    private Role role;         //用户角色id

    private int createby;           //创建人id

    private Date creationdate;      //创建时间

    private int modifyby;           //修改人

    private Date modifydate;        //修改时间

    private String cardphoto;       //证件照地址

    private String workphoto;       //工作照地址

    public String getCardphoto() {
        return cardphoto;
    }

    public void setCardphoto(String cardphoto) {
        this.cardphoto = cardphoto;
    }

    public String getWorkphoto() {
        return workphoto;
    }

    public void setWorkphoto(String workphoto) {
        this.workphoto = workphoto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getCreateby() {
        return createby;
    }

    public void setCreateby(int createby) {
        this.createby = createby;
    }

    public Date getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(Date creationdate) {
        this.creationdate = creationdate;
    }

    public int getModifyby() {
        return modifyby;
    }

    public void setModifyby(int modifyby) {
        this.modifyby = modifyby;
    }

    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }
}
