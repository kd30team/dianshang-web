package com.kgc.servlet;

import com.alibaba.fastjson.JSONObject;
import com.kgc.entity.Role;
import com.kgc.service.RoleService;
import com.kgc.service.impl.RoleServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class RoleServlet extends HttpServlet {

    private RoleService roleService = new RoleServiceImpl();

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String mode = request.getParameter("mode");
        if("list".equals(mode)){
            this.listRole(request, response);
        }
    }

    private  void listRole(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //获取数据
        List<Role> list = roleService.listRole();
        //转json
        String json = JSONObject.toJSONString(list);
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        out.print(json);
        out.flush();
        out.close();
    }

}
