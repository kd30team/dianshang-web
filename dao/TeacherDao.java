package dao;

import entiy.Teacher;

public interface TeacherDao {
    Teacher selectByUsernamePassword(String username, String password);
}
