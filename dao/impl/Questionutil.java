package dao.impl;





import utill.Mannagr;

import java.sql.*;

public class Questionutil {
    private static  String DRIVER_CLASS;
    private static  String URL;
    private static  String USERNAME;
    private static  String PASSWORD;

    Connection conn=null;
    PreparedStatement ps=null;
    ResultSet rs=null;


    static {
        try {
            Mannagr cm =Mannagr.getManager ();
            DRIVER_CLASS=cm.getValue ( "driver" );
            URL=cm.getValue ( "url" );
            USERNAME=cm.getValue ( "username" );
            PASSWORD=cm.getValue ( "password" );
            Class.forName ( DRIVER_CLASS);
        } catch (ClassNotFoundException e) {
            e.printStackTrace ();
        }
    }


    Connection getConnection(){
        try {
            conn = DriverManager.getConnection ( URL , USERNAME , PASSWORD );
        } catch (SQLException throwables) {
            throwables.printStackTrace ();
        }
        return conn;
    }



    ResultSet getResultSet(String sql,Object[] obj){
        try {
            conn = this.getConnection ();
            ps = conn.prepareStatement ( sql );
            if (obj != null){
                for (int i=0;i<obj.length;i++){
                    ps.setObject ( i+1,obj[i] );
                }
            }
            rs=ps.executeQuery ();
        } catch (SQLException e) {
            e.printStackTrace ();
        }
        return  rs;

    }

    int executeUpdate(String sql , Object[] obj) {
        int count = 0;
        try {
            conn = this.getConnection ();
            ps = conn.prepareStatement ( sql );
            if (obj != null) {
                for (int i = 0; i < obj.length; i++) {
                    ps.setObject ( i + 1 , obj[i] );
                }
            }
            count = ps.executeUpdate ();
        } catch (SQLException e) {
            e.printStackTrace ();
        } finally {
        }

        return count;
    }

    void closeAll(){
        try {
            if (rs !=null){
                rs.close ();
            }
            if (ps !=null){
                ps.close ();
            }
            if (conn !=null){
                conn.close ();
            }
        } catch (SQLException e) {
            e.printStackTrace ();
        }
    }

}
