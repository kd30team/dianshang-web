package dao.impl;

import dao.TeacherDao;
import entiy.Teacher;

import java.sql.SQLException;

public class TeacherImpl extends Questionutil implements TeacherDao {
    @Override
    public Teacher selectByUsernamePassword(String username, String password) {
        Teacher teacher=null;
        String sql="select * from teacher where username=?,password=?";
        rs=this.getResultSet(sql,new Object[]{username,password});
        try {
            if(rs.next()){
                teacher=new Teacher();
                teacher.setId(rs.getInt("id"));
                teacher.setUsername(rs.getString("username"));
                teacher.setPassword(rs.getString("password"));
                teacher.setPhone(rs.getString("phone"));
                teacher.setEmail(rs.getString("email"));
                teacher.setDop(rs.getString("dop"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            this.closeAll();
        }
        return teacher;
    }
}
