package dao;

import entiy.Question;

import java.util.List;

public interface QuestionDao {
    List<Question> selectQuestion();
    public int add(Question q);
    int deleteQuestion(int id);
    Question selectbyidQuestion(int id);
    int updateQuestion(Question q);
}
